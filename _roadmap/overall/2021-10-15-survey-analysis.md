---
date: 2021-10-15
title: Tasks in October 2021
service: overall
---

## Primary Analysis Results of [HIFIS Survey 2021]({% link services/overall/survey.html %})
Data collection and primary analysis of the HIFIS survey 2021 has been finished. 
First results will be presented in respective meetings (e.g., HIFIS conference, Incubator workshop).
