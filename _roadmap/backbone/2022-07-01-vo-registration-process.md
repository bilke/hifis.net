---
date: 2022-07-01
title: Tasks in July 2022
service: backbone
---

## AAI: Establish improved process for Virtual Organisation Registration
Virtual Organisations (VOs) have been introduced in Helmholtz AAI during 2020.
Requests to create new top-level VOs are currently being processed on end user request,
using the HIFIS Helpdesk and considerable manual interaction for VO creation and documentation.
In 2022, a transparent process shall be set up and implemented to allow VO handling in larger scales than so far.
