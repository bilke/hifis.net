---
date: 2020-11-01
title: Tasks in November 2020
service: software
---

## Integrate the Software Management Platform into the Helmholtz Cloud
The software management platform is planned to be integrated into the
Helmholtz Cloud as soon as the technical platform is available.
Continuous Integration will be part of the software management platform from
the very beginning.

## Recipes for a Scalable CI solution
One important element of the software management platform is a solution for
[continuous integration (CI)](https://en.wikipedia.org/wiki/Continuous_integration).
Reusable Ansible recipes for a GitLab CI infrastructure will be provided.
The Ansible role is developed [here](https://gitlab.com/hifis/ansible/gitlab-ci-openstack).
