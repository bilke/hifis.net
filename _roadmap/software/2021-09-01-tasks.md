---
date: 2021-09-01
title: Tasks in September 2021
service: software
---

## CI as a Service (Pilot)
We consider Continuous Integration (CI) an important aspect in a
modern software engineering workflow.
Easy-to-use CI resources should be available for all Helmholtz scientists.
Beside the [Helmholtz-wide GitLab](https://gitlab.hzdr.de) many Helmholtz
centers run their own GitLab instances on dedicated infrastructure in their
center.
In order to bundle resources and enable research software projects
to easily use CI for their projects, CI will be offered as a service.
In this milestone, we will set up CI as a Service and test it first with few
pilot centers, in order to make it available to all Helmholtz communities.

Please note: The milestone was originally set for the end of the second quarter.
Due to the underlying compute resources not being available in time, this
milestone needed to be rescheduled to the third quarter of 2021.
