---
title: "New HIFIS course: Introduction to OOP in Python"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash_shrinked.jpg
date: 2022-05-11
authors:
 - klaffki
layout: blogpost
categories:
  - news
excerpt: >
  HIFIS Education & Training developed a new online course which will take place for the first time May 23–24, 2022: Object-Orientated Programming in Python.
---

## New HIFIS course: Object-Orientated Programming in Python
HIFIS Education & Training developed a new online course which will take place for the first time **May 23–24, 2022**: Object-Orientated Programming in Python. It aims at intermediate level users as some basic knowledge of Python is already required (variables, functions, loops, conditions). For details of the agenda have a look at the [course page](https://events.hifis.net/event/427/timetable/#20220523.detailed).

## Object-Orientated Programming
Object-Orientated Programming (OOP) is a popular and widely used programming paradigm where programs are written to describe the involved elements and the interactions between these. This is a popular approach for modelling and simulation problems. Among the languages that use OOP are Java, C++, Ruby — and Python. The latter will be used in this class to teach the concept.

## Registration is open!
The registration for this course is open until Sunday 15th of May, so do not hesitate to [secure your place](https://events.hifis.net/event/427/registrations/408/)! But please note, this is the first installment of this workshop. Bugs and hick-ups are to be expected. The instructors recommend to bring along some patience and popcorn ;)
