---
title: Welcome to HIFIS Software
date: 2019-12-20
authors:
  - huste
  - Leinweber, Katrin
layout: blogpost
title_image: photo-of-open-signage-2763246.jpg
excerpt:
  "We are very happy to announce the initial release of software.hifis.net:
  the central platform for distributing information, promoting events and
  offering tutorials and content concerning scientific software development
  within Helmholtz."
categories:
  - blog
tags:
  - general
---

# Welcome to HIFIS Software
We are very happy to announce the initial release of **software.hifis.net**:
The central platform for distributing information, promoting events and
offering tutorials and content concerning scientific software development
within Helmholtz.

## Who are we?
HIFIS or Helmholtz Federated IT Services is one of 5 platforms initiated by
the [Helmholtz Incubator][incubator] besides

* [HIDA - the Helmholtz Information & Data Science Academy][hida-link],
* [Helmholtz AI][haicu-link],
* [HMC - the Helmholtz Metadata Collaboration Platform][hmc-link] and
* [HIP - the Helmholtz Imaging Platform][hmc-link].

HIFIS aims to install a secure and user-friendly collaborative environment
offering information and communication technology services that are accessible
from anywhere.
The platform also supports the development of high-quality, secure, and sustainable
research software.

These objectives are implemented through three competence clusters:

- The [Cloud Services Cluster][cloud-link] providing a federated platform for proven,
  first-class cloud services,
- A [Backbone Services Cluster][backbone-link] providing a powerful and reliable network
  infrastructure with uniform basic services and
- Us, the Software Services Cluster providing a platform, training, and support
  for high-quality, sustainable software development.

Our mission is to empower scientists of any domain to implement and to
perpetuate modern scientific software development principles in order to make
research software engineering more sustainable. Our core team is located in 5
different Helmholtz Centers:

- [DLR - German Aerospace Center][dlr]
- [DKFZ - German Cancer Research Center][dkfz]
- [GFZ - German Research Centre for Geosciences][gfz]
- [HZDR - Helmholtz-Zentrum Dresden-Rossendorf][hzdr]
- [UFZ - Helmholtz Centre for Environmental Research][ufz]

Have a look into our [team page]({{ '/team' | relative_url }}).
You will find an interactive map to see how we are spread over Germany.
Since we aim to provide services for the **whole** Helmholtz Association,
we would be more than happy, if **you** would like to act
as our [contact person](#contact-us) in **your** Helmholtz center.
Help us spread the word to make research software development more sustainable.

## What are we going to offer?

Please find that information on our [services page]({{ '/services/' | relative_url }}).

Stay tuned! The first training events, services and materials
will be announced there in early 2020.

## How to get involved?

Distributing information about services offered, upcoming training events or
building Helmholtz-wide communities requires us to have **you** as a
motivated contact person in your research center or research group who is
willing to help in communicating and spreading the word about sustainable
software development and HIFIS.

By using a clearly defined version control workflow contributing is made easy for everyone.
Therefore, you can [contribute to this website][gitlab-project].
If your software project was improved by a HIFIS-provided training or advice,
or want to share an interesting solution or use case,
please feel free to write or re-blog a short post about it here.
You can even add yourself to our team page as an associate.
Our [Contribution Guide][contributing] explains how to contribute to *hifis.net*.

You are also welcome to [tell us](#contact-us) your interest and we will keep you up-to-date about upcoming
trainings, events and services in 2020 and forward.

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Contact us</h2>
  <p>
    Send a mail to
    <strong>
      <a href="mailto:{{ site.contact_mail }}">{{ site.contact_mail }}</a>.
    </strong>
  </p>
</div>

[incubator]: https://www.helmholtz.de/en/research/information-data-science/helmholtz-incubator/
[hida-link]: https://www.helmholtz-hida.de/
[haicu-link]: http://www.helmholtz.ai/
[hmc-link]: https://www.helmholtz.de/en/research/information-data-science/helmholtz-metadata-collaboration-plattform-hmc/
[hip-link]: https://www.helmholtz.de/en/research/information-data-science/helmholtz-imaging-platform-hip/
[dlr]: https://dlr.de/sc
[dkfz]: https://www.dkfz.de/
[gfz]: https://www.gfz-potsdam.de/zentrum/escience-zentrum/ueberblick/
[hzdr]: https://www.hzdr.de/
[ufz]: https://www.ufz.de/
[contributing]: https://gitlab.hzdr.de/hifis/software.hifis.net/blob/master/CONTRIBUTING.md
[cloud-link]: https://www.hifis.net/mission/cluster-cloud.html
[backbone-link]: https://www.hifis.net/mission/cluster-backbone.html
[gitlab-project]: https://gitlab.hzdr.de/hifis/software.hifis.net
