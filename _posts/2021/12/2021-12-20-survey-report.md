---
title: "HIFIS Survey 2021 Report"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash.jpg
date: 2021-12-20
authors:
  - jandt
layout: blogpost
categories:
  - news
redirect_from:
  - survey2021
excerpt: >
  During 2021, HIFIS conducted a survey throughout the Helmholtz Association, querying the needs and current practices of all Helmholtz colleagues in using IT services.
  The analysis and report are now online.

---

# Why a survey?

HIFIS provides numerous IT services for Helmholtz and collaboration partners, ranging from cloud and technology services to education, consulting, and community building.
From users of these services, we obtain feedback on the quality and potential future developments via various channels.
On top of that, we are especially interested in obtaining information from colleagues in Helmholtz that are *not* already closely related to HIFIS services.
We would like to know more on users' needs and wishes, and maybe organisational bottlenecks, so we can address these in our future service portfolio.

With this motivation in mind, we surveyed a randomly selected subset of employees of the participating Helmholtz centres (15 of 18 queried centres plus Head Office took part) and asked them to anonymously report on their usage and practices of IT services in their field of work, also on software development practices, if that applied for the questioned user.

### Results

We were able to obtained responses from 15 Helmholtz centres and the Head Office.
A total of 594 participants responded to the HIFIS Survey 2021 between May and August 2021.
After filtering for inconsistent and too incomplete responses, a total of 411 responses were analyzed.

Approximately 55% of them were from researchers, the remaining fraction was distributed between Administrative, Infrastructure and IT staff.
All six research areas were represented.


Results are presented and discussed in an [**analysis report that is available here**](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis/-/jobs/artifacts/master/raw/report/hifis-survey-2021.pdf?job=build:graphsreport).

### Data and Analysis Repositories

The data and the complete analysis pipeline as well as the report sources are available publicly.
You are invited to check the data and/or do your own analyses.
If you happen to find new interesting results, we'd appreciate [if you tell us](mailto:support@hifis.net)!

* The data set, excluding free text answers, is provided here: <https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/data>
* The analysis scripts and report sources are located here: <https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis>
* The analysis backend is built on the HIFIS Surveyval Framework, which can be found here: <https://gitlab.hzdr.de/hifis/overall/surveys/hifis-surveyval>

[Write us](mailto:support@hifis.net) if you are interested on details for these.

### Further analysis

Based on the primary analysis summarised in the report, it can already be stated that further
information can likely be derived for specific sub-groups in additional analyses.
This may yield useful insights on how to shape HIFIS services towards these groups.

<i class="fas fa-bell"></i> **Stay tuned!** <i class="fas fa-bell"></i>

### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>


